package pet.dmitry.recyclerSample.data

import android.support.test.runner.AndroidJUnit4
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import pet.dmitry.recyclerSample.App

/**
 * @author Dmitry Borodin on 11/20/18.
 */
@RunWith(AndroidJUnit4::class)
class MessageExternalProviderTest {

    private lateinit var messageExternalProvider : MessageExternalProvider

    @Before
    fun setUp() {
        messageExternalProvider = MessageExternalProvider(App.instance.applicationContext)
    }

    @Test
    fun testGettingMessagesAndUsers() {
        val data = messageExternalProvider.getFullMessageList()
        Assert.assertTrue(data.messages.isNotEmpty())
        Assert.assertTrue(data.users.isNotEmpty())
    }

    @Test
    fun testSettingUserIdForAttachments() {
        val data = messageExternalProvider.getFullMessageList()
        data.messages.forEach { it.attachments?.forEach {
            Assert.assertNotEquals(it.messageId, Long.MIN_VALUE)
        } }
    }

}