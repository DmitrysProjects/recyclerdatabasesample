package pet.dmitry.recyclerSample

import android.app.Application
import androidx.room.Room
import com.squareup.picasso.Picasso
import pet.dmitry.recyclerSample.data.Database
import pet.dmitry.recyclerSample.data.MessageExternalProvider
import pet.dmitry.recyclerSample.data.MessageRepository
import timber.log.Timber

/**
 * @author Dmitry Borodin on 11/20/18.
 */
class App : Application() {

    private val database: Database by lazy {
        Room.databaseBuilder(
            applicationContext,
            Database::class.java,
            "AppDatabase"
        ).build()
    }

    val repository: MessageRepository by lazy {
        MessageRepository(
            externalProvider = MessageExternalProvider(this.applicationContext),
            database = database
        )
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
            Picasso.get().isLoggingEnabled = true
        }
    }

    companion object {
        @Volatile
        lateinit var instance: App
    }
}