package pet.dmitry.recyclerSample.base

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlin.coroutines.CoroutineContext

/**
 * @author Dmitry Borodin on 11/20/18.
 */
abstract class ScopedPresenter : CoroutineScope {

    protected lateinit var job: Job
    override val coroutineContext: CoroutineContext
        get() = job + Dispatchers.Main

    open fun onAttach() {
        job = Job()
    }

    open fun onDetouch() {
        job.cancel()
    }
}