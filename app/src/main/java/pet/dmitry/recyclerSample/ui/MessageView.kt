package pet.dmitry.recyclerSample.ui

/**
 * @author Dmitry Borodin on 11/20/18.
 */
interface MessageView {
    fun showInitialList(messageList: List<ChatUiModel>)

    fun addNewerMessages(messageList: List<ChatUiModel>)

    fun addOlderMessages(messageList: List<ChatUiModel>)
}