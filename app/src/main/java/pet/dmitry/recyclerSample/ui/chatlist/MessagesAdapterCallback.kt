package pet.dmitry.recyclerSample.ui.chatlist

/**
 * @author Dmitry Borodin on 11/20/18.
 */
interface MessagesAdapterCallback {
    fun onNewestReached(newestAvailable: Long)
    fun onOldestReached(oldestAvailable: Long)
    fun onMessageRemoved(messageId: Long)
    fun onAttachmentRemoved(attachId: String)
}