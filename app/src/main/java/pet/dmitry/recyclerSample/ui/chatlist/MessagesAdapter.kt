package pet.dmitry.recyclerSample.ui.chatlist

import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.viewbinder.bindView
import pet.dmitry.recyclerSample.R
import pet.dmitry.recyclerSample.base.CircleTransformation
import pet.dmitry.recyclerSample.ui.AttachmentUiModel
import pet.dmitry.recyclerSample.ui.ChatUiModel
import pet.dmitry.recyclerSample.ui.MessageUiModel
import timber.log.Timber

/**
 * @author Dmitry Borodin on 11/20/18.
 */
private const val BOUNDARY_THRESHOLD = 0

class MessagesAdapter(private val chatAdapterCallback: MessagesAdapterCallback) :
    RecyclerView.Adapter<ChatViewHolder>() {

    init {
        setHasStableIds(true)
    }

    var items: MutableList<ChatUiModel> = mutableListOf()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    fun addNewerMessages(chatElements: List<ChatUiModel>) {
        if (chatElements.isEmpty()) return
        items.addAll(chatElements)
        notifyItemRangeInserted(items.size - chatElements.size, chatElements.size)
    }

    fun addOlderMessages(chatElements: List<ChatUiModel>) {
        if (chatElements.isEmpty()) return
        items.addAll(0, chatElements)
        notifyItemRangeInserted(0, chatElements.size)
    }

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        ItemTouchHelper(object : SwipeHelper() {
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                when (viewHolder) {
                    is MessageViewHolder -> {
                        val index = viewHolder.adapterPosition
                        chatAdapterCallback.onMessageRemoved((items[index] as MessageUiModel).id)
                        var removingSize = 1
                        while (items.lastIndex >= index + removingSize
                            && items[index + removingSize] is AttachmentUiModel
                        ) {
                            removingSize++
                        }
                        repeat(removingSize) {
                            items.removeAt(index)
                        }
                        notifyItemRangeRemoved(index, removingSize)
                    }
                    is AttachmentViewHolder -> {
                        val index = viewHolder.adapterPosition
                        chatAdapterCallback.onAttachmentRemoved((items[index] as AttachmentUiModel).attachmentId)
                        items.removeAt(index)
                        notifyItemRemoved(index)
                    }
                }
            }
        }).attachToRecyclerView(recyclerView)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChatViewHolder {
        Timber.i("onCreateViewHolder() viewtype $viewType")
        return when (viewType) {
            R.layout.item_message_mine -> {
                val view = LayoutInflater.from(parent.context).inflate(R.layout.item_message_mine, parent, false)
                MessageMeViewHolder(view)
            }
            R.layout.item_message_others -> {
                val view = LayoutInflater.from(parent.context).inflate(R.layout.item_message_others, parent, false)
                MessageOtherViewHolder(view)
            }
            R.layout.item_message_attachment -> {
                val view = LayoutInflater.from(parent.context).inflate(R.layout.item_message_attachment, parent, false)
                AttachmentViewHolder(view)
            }
            else -> throw RuntimeException("Unknown viewtype in adapter")
        }
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ChatViewHolder, position: Int) {
        Timber.i("onBindViewHolder() holder ${holder.javaClass} position $position")

        if (position == 0 + BOUNDARY_THRESHOLD) chatAdapterCallback.onOldestReached(getOldestMessageId())
        if (position == items.lastIndex - BOUNDARY_THRESHOLD) chatAdapterCallback.onNewestReached(getNewestMessageId())

        holder.bind(items[position])
    }

    private fun getNewestMessageId(): Long {
        val oldestMessage = items.last { it is MessageUiModel }
        return oldestMessage.id
    }

    private fun getOldestMessageId(): Long {
        val newestMessage = items.first { it is MessageUiModel }
        return newestMessage.id
    }

    override fun getItemId(position: Int): Long = items[position].id

    override fun getItemViewType(position: Int): Int {
        val item = items[position]
        return when (item) {
            is MessageUiModel -> if (item.isMine) R.layout.item_message_mine else R.layout.item_message_others
            is AttachmentUiModel -> R.layout.item_message_attachment
        }
    }
}

abstract class ChatViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    abstract fun bind(model: ChatUiModel)
}

abstract class MessageViewHolder(view: View) : ChatViewHolder(view) {
}

class MessageMeViewHolder(val view: View) : MessageViewHolder(view) {

    private val content by lazy { view.findViewById(R.id.item_message_mine_content) as TextView }

    override fun bind(model: ChatUiModel) {
        val messageModel = model as MessageUiModel
        content.text = messageModel.messageContent
    }

}

class MessageOtherViewHolder(val view: View) : MessageViewHolder(view) {
    private val avatar by bindView<ImageView>(R.id.item_message_others_avatar)
    private val content by bindView<TextView>(R.id.item_message_others_content)
    private val username by bindView<TextView>(R.id.item_message_others_username)

    override fun bind(model: ChatUiModel) {
        val messageModel = model as MessageUiModel
        Picasso.get()
            .load(messageModel.avatarUrl)
            .fit()
            .transform(CircleTransformation())
            .placeholder(R.drawable.ic_person_black_24dp)
            .noFade()
            .into(avatar)
        content.text = messageModel.messageContent
        username.text = messageModel.username
    }
}


class AttachmentViewHolder(val view: View) : ChatViewHolder(view) {
    private val imageView by bindView<ImageView>(R.id.item_message_attachment_image)
    private val parentView by bindView<LinearLayout>(R.id.item_message_attachment_parent)
    private val title by bindView<TextView>(R.id.item_message_attachment_title)

    override fun bind(model: ChatUiModel) {
        val attachmentModel = model as AttachmentUiModel
        Picasso.get()
            .load(attachmentModel.thumbnailUrl)
            .fit()
            .into(imageView)
        title.text = attachmentModel.title
        val params = parentView.layoutParams as FrameLayout.LayoutParams
        params.gravity = if (attachmentModel.isMine) Gravity.RIGHT else Gravity.LEFT
    }
}