package pet.dmitry.recyclerSample.ui

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import pet.dmitry.recyclerSample.App
import pet.dmitry.recyclerSample.base.ScopedPresenter
import timber.log.Timber

/**
 * @author Dmitry Borodin on 11/20/18.
 */
class MessagePresenter : ScopedPresenter() {

    val repository = App.instance.repository
    var view: MessageView? = null

    fun onAttach(view: MessageActivity) {
        super.onAttach()
        this.view = view
    }

    override fun onDetouch() {
        super.onDetouch()
        this.view = null
    }

    fun requestInitialMessages() {
        launch {
            val messages = repository.getInitialMessages()
            view?.showInitialList(messages)
        }
    }

    fun requestNewerMessages(newestAvailable: Long) {
        Timber.d("requestNewerMessages() $newestAvailable")
        launch {
            val messages = repository.getNewerMessages(newestAvailable)
            view?.addNewerMessages(messages)
        }
    }

    fun requestOlderMessages(oldestAvailable: Long) {
        Timber.d("requestOlderMessages() $oldestAvailable")
        launch {
            val messages = repository.getOlderMessages(oldestAvailable)
            view?.addOlderMessages(messages)
        }
    }

    fun messageRemoved(messageId: Long) = GlobalScope.launch(Dispatchers.Default) {
        repository.deleteMessage(messageId)
    }

    fun attachRemoved(attachId: String) = GlobalScope.launch(Dispatchers.Default) {
        repository.deleteAttachment(attachId)
    }
}