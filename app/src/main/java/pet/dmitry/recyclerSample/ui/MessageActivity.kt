package pet.dmitry.recyclerSample.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.viewbinder.bindView
import pet.dmitry.recyclerSample.R
import pet.dmitry.recyclerSample.ui.chatlist.MessagesAdapter
import pet.dmitry.recyclerSample.ui.chatlist.MessagesAdapterCallback

class MessageActivity : AppCompatActivity(), MessageView {

    private val recycler by bindView<RecyclerView>(R.id.activity_main_recycler)

    private val adapterCallback = object : MessagesAdapterCallback {
        override fun onNewestReached(newestAvailable: Long) {
            presenter.requestNewerMessages(newestAvailable)
        }

        override fun onOldestReached(oldestAvailable: Long) {
            presenter.requestOlderMessages(oldestAvailable)
        }

        override fun onMessageRemoved(messageId: Long) {
            presenter.messageRemoved(messageId)
        }

        override fun onAttachmentRemoved(attachId: String) {
            presenter.attachRemoved(attachId)
        }
    }
    private val presenter = MessagePresenter()
    private val adapter = MessagesAdapter(adapterCallback)


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        presenter.onAttach(this)
        recycler.adapter = adapter
        recycler.layoutManager = LinearLayoutManager(this).apply {
            stackFromEnd = true
        }
        presenter.requestInitialMessages()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onDetouch()
    }

    override fun showInitialList(messageList: List<ChatUiModel>) {
        adapter.items = messageList.toMutableList()
    }

    override fun addNewerMessages(messageList: List<ChatUiModel>) {
        adapter.addNewerMessages(messageList)
    }

    override fun addOlderMessages(messageList: List<ChatUiModel>) {
        adapter.addOlderMessages(messageList)
    }
}
