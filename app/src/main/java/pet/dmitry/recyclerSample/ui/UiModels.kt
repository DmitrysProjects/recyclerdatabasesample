package pet.dmitry.recyclerSample.ui

/**
 * @author Dmitry Borodin on 11/20/18.
 *
 * ID of Chat model has to be unique, in order to recycler to work in stable IDs mode
 */

sealed class ChatUiModel(open val id: Long)

data class MessageUiModel(
    val messageContent: String,
    override val id: Long,
    val isMine: Boolean,
    val username: String = "",
    val avatarUrl: String = ""
) : ChatUiModel(id)

data class AttachmentUiModel(
    val title: String,
    val thumbnailUrl: String,
    val messageId: Long,
    val attachmentId: String,
    val isMine: Boolean,
    override val id: Long
) : ChatUiModel(id)