package pet.dmitry.recyclerSample.data

import androidx.room.*

/**
 * @author Dmitry Borodin on 11/20/18.
 */
@Dao
interface ChatFullDao {

    @Query("SELECT * FROM message")
    fun _getAllmessages(): MutableList<Message>

    @Query("SELECT * FROM user")
    fun getAllUsers(): List<User>

    @Query("SELECT * FROM attachment WHERE messageId = :messageId")
    fun _getAttachemntsForMessage(messageId: Long): List<Attachment>

    @Transaction
    fun getMessagesWithAttachments() : List<Message> {
        return _getAllmessages().apply {
            forEach { it.attachments = _getAttachemntsForMessage(it.id) }
            sortBy { it.id }
        }
    }

    @Transaction
    fun insertMessagesWithAttachments(messages: List<Message>) {
        _insertMessagesWithoutAttach(messages)
        messages.forEach { message ->
            message.attachments?.let { _insertAttachments(it) }
        }
    }

    @Insert(onConflict = OnConflictStrategy.FAIL) //should never happen
    fun _insertMessagesWithoutAttach(messages: List<Message>)

    @Insert(onConflict = OnConflictStrategy.FAIL) //should never happen
    fun _insertAttachments(attachments: List<Attachment>)

    @Insert(onConflict = OnConflictStrategy.FAIL) //should never happen
    fun insertUsers(users: List<User>)

    @Query("DELETE FROM message WHERE id = :messageId")
    fun _deleteMessage(messageId: Long)

    @Query("DELETE FROM attachment WHERE messageId = :messageId")
    fun _deleteAttachmentsForMessage(messageId: Long)

    @Query("DELETE FROM attachment WHERE id = :attachmentId")
    fun deleteAttachment(attachmentId : String)

    @Transaction
    fun deleteMessageWithAttachments(messageId: Long) {
        _deleteAttachmentsForMessage(messageId)
        _deleteMessage(messageId)
    }
}