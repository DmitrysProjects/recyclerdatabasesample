package pet.dmitry.recyclerSample.data

import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

/**
 * @author Dmitry Borodin on 11/20/18.
 */

data class ExternalMessagesResponse(
    val messages: List<Message>,
    val users: List<User>
)

@Entity
data class User(
    @PrimaryKey
    val id: Long,
    val name: String,
    @SerializedName("avatarId")
    val avatarUrl: String
)

@Entity
data class Message(
    @PrimaryKey
    var id: Long,
    var userId: Long,
    var content: String
) {
    //Room cannot use constructor with Ignored field. Fix when relations for entities will be supported by ROOM
    @Ignore
    var attachments: List<Attachment>? = null
}

@Entity
data class Attachment(
    @PrimaryKey
    val id: String,
    val title: String,
    val url: String,
    val thumbnailUrl: String
) {
    var messageId: Long = Long.MIN_VALUE
}
