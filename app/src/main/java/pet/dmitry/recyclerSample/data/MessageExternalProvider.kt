package pet.dmitry.recyclerSample.data

import android.content.Context
import com.google.gson.Gson
import com.google.gson.stream.JsonReader
import pet.dmitry.recyclerSample.R
import java.io.InputStreamReader


/**
 * @author Dmitry Borodin on 11/20/18.
 */

class MessageExternalProvider(private val applicationContext: Context) {

    fun getFullMessageList(): ExternalMessagesResponse {
        val gson = Gson()
        val reader = JsonReader(InputStreamReader(applicationContext.resources.openRawResource(R.raw.data)))
        val response: ExternalMessagesResponse = gson.fromJson(reader, ExternalMessagesResponse::class.java)
        for (message in response.messages) {
            message.attachments?.forEach { it.messageId = message.id }
        }
        return response
    }
}