package pet.dmitry.recyclerSample.data

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import pet.dmitry.recyclerSample.ui.AttachmentUiModel
import pet.dmitry.recyclerSample.ui.ChatUiModel
import pet.dmitry.recyclerSample.ui.MessageUiModel
import timber.log.Timber
import java.util.concurrent.atomic.AtomicLong

/**
 * @author Dmitry Borodin on 11/20/18.
 */

private const val MY_USER_ID = 1L

class MessageRepository(
    val externalProvider: MessageExternalProvider,
    val database: Database
) {

    private val chatFullDao = database.getChatFullDao()
    private val chatPartialDao = database.getChatPartialDao()

    private val lazyAllUsers: Map<Long, User> by lazy { chatFullDao.getAllUsers().map { it.id to it }.toMap() }

    private var attachUniqueId = AtomicLong(-1L)
    private fun getNextAttachId(): Long = attachUniqueId.decrementAndGet()

    suspend fun getAllMessages(): List<ChatUiModel> = withContext(Dispatchers.IO) {
        Timber.i("get all messages")
        val messages = chatFullDao.getMessagesWithAttachments()
        if (messages.isEmpty()) {
            Timber.d("database is empty, fetching data")
            val data = externalProvider.getFullMessageList()
            launch {
                chatFullDao.insertMessagesWithAttachments(data.messages)
                chatFullDao.insertUsers(data.users)
            }
            return@withContext convertToUiModels(data.messages, data.users.map { it.id to it }.toMap())
        }
        return@withContext convertToUiModels(messages, lazyAllUsers)
    }

    private fun convertToUiModels(messages: List<Message>, users: Map<Long, User>): MutableList<ChatUiModel> {
        val result = mutableListOf<ChatUiModel>()
        for (message in messages) {
            val author = users[message.userId]!!
            result.add(
                MessageUiModel(
                    messageContent = message.content,
                    id = message.id,
                    isMine = message.userId == MY_USER_ID,
                    username = author.name,
                    avatarUrl = author.avatarUrl
                )
            )
            message.attachments?.forEach {
                result.add(
                    AttachmentUiModel(
                        title = it.title,
                        id = getNextAttachId(),
                        isMine = message.userId == MY_USER_ID,
                        attachmentId = it.id,
                        messageId = message.id,
                        thumbnailUrl = it.thumbnailUrl
                    )
                )
            }
        }
        return result
    }

    suspend fun getInitialMessages(): List<ChatUiModel> = withContext(Dispatchers.IO) {
        Timber.i("get initial messages")
        val messages = chatPartialDao.getInitialMessagesWithAttachments()
        if (messages.isEmpty()) {
            Timber.i("database is empty, fetching data")
            val data = externalProvider.getFullMessageList()
            launch {
                chatFullDao.insertMessagesWithAttachments(data.messages)
                chatFullDao.insertUsers(data.users)
            }
            return@withContext convertToUiModels(data.messages, data.users.map { it.id to it }.toMap())
        }
//        Timber.i("messagesLoaded ${messages.map { it.id }}")
        return@withContext convertToUiModels(messages, lazyAllUsers)
    }

    suspend fun getNewerMessages(newestAvailableId: Long): List<ChatUiModel> = withContext(Dispatchers.IO) {
        val messages = chatPartialDao.getNewestMessagesWithAttachments(newestAvailableId)
//        Timber.i("messagesLoaded ${messages.map { it.id }}, newestAvailableId $newestAvailableId")
        return@withContext convertToUiModels(messages, lazyAllUsers)
    }

    suspend fun getOlderMessages(olestAvailableId: Long): List<ChatUiModel> = withContext(Dispatchers.IO) {
        val messages = chatPartialDao.getOlderMessagesWithAttachments(olestAvailableId)
//        Timber.i("messagesLoaded ${messages.map { it.id }}, olestAvailableId $olestAvailableId")
        return@withContext convertToUiModels(messages, lazyAllUsers)
    }

    suspend fun deleteMessage(messageId: Long) = withContext(Dispatchers.IO) {
        return@withContext chatFullDao.deleteMessageWithAttachments(messageId)
    }

    suspend fun deleteAttachment(attachmentId: String) = withContext(Dispatchers.IO) {
        chatFullDao.deleteAttachment(attachmentId)
    }
}