package pet.dmitry.recyclerSample.data

import androidx.room.Database
import androidx.room.RoomDatabase

/**
 * @author Dmitry Borodin on 11/20/18.
 */
@Database(entities = [(User::class), (Message::class), (Attachment::class)], version = 1, exportSchema = false)
abstract class Database : RoomDatabase() {

    abstract fun getChatFullDao(): ChatFullDao

    abstract fun getChatPartialDao(): ChatRangedDao
}
