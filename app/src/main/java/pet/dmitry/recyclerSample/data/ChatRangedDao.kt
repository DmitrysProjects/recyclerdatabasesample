package pet.dmitry.recyclerSample.data

import androidx.room.Dao
import androidx.room.Query
import androidx.room.Transaction

/**
 * @author Dmitry Borodin on 11/20/18.
 */

private const val PAGINATION_SIZE = 20

@Dao
interface ChatRangedDao {

    @Query("SELECT * FROM message ORDER BY id DESC LIMIT :limit")
    fun _getInitialMessages(limit: Int): MutableList<Message>

    @Query("SELECT * FROM user")
    fun getAllUsers(): List<User>

    @Query("SELECT * FROM attachment WHERE messageId = :messageId")
    fun _getAttachemntsForMessage(messageId: Long): List<Attachment>

    @Transaction
    fun getInitialMessagesWithAttachments(): List<Message> {
        return _getInitialMessages(PAGINATION_SIZE).apply {
            forEach { it.attachments = _getAttachemntsForMessage(it.id) }
            sortBy { it.id }
        }
    }

    @Query("SELECT * FROM message WHERE (id > :newestAvailable ) ORDER BY id ASC LIMIT :limit")
    fun _getNewestMessages(newestAvailable: Long, limit: Int): MutableList<Message>

    @Transaction
    fun getNewestMessagesWithAttachments(newestAvailable: Long): List<Message> {
        return _getNewestMessages(newestAvailable, PAGINATION_SIZE).apply {
            forEach { it.attachments = _getAttachemntsForMessage(it.id) }
            sortBy { it.id }
        }
    }

    @Query("SELECT * FROM message WHERE (id < :oldestAvailable) ORDER BY id DESC LIMIT :limit")
    fun _getOlderMessages(oldestAvailable: Long, limit: Int): MutableList<Message>

    @Transaction
    fun getOlderMessagesWithAttachments(oldestAvailable: Long): List<Message> {
        return _getOlderMessages(oldestAvailable, PAGINATION_SIZE).apply {
            forEach { it.attachments = _getAttachemntsForMessage(it.id) }
            sortBy { it.id }
        }
    }
}